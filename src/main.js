import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueSocketio from 'vue-socket.io'
import VueYoutube from 'vue-youtube'
import './registerServiceWorker'

Vue.config.productionTip = false
Vue.use(VueSocketio, '/')
Vue.use(VueYoutube)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
