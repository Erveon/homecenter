import Vue from 'vue'
import Router from 'vue-router'
import Control from './views/Control.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
	{
		path: '/',
		name: 'control',
		component: Control
	},
	{
		path: '/view',
		name: 'viewcenter',
		// route level code-splitting
		// this generates a separate chunk (view.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import(/* webpackChunkName: "viewcenter" */ './views/ViewCenter.vue')
	}
  ]
})
