module.exports = {
	productionSourceMap: false,
	devServer: {
		disableHostCheck: true,
		proxy: {
			'/socket.io': {
				target: 'http://localhost:3487',
				changeOrigin: true
			}
		},
	}
}