const express = require('express')
const path = require('path')
const app = express()
const bodyParser = require('body-parser')
const server = require('http').createServer(app)
const io = require('socket.io')(server)

process.on('unhandledRejection', (reason, p) => {
	console.log('Unhandled Rejection at:', p, 'reason:', reason)
})

process.on('uncaughtException', err => {
	console.error(err)
})

const views = {}

io.on('connection', (socket) => {
    socket.on('i am a view', () => {
        console.log("A view registered itself")
        views[socket.id] = socket
    })
    socket.on('play', (videoId, videoTitle) => {
       for(let id in views) {
           const view = views[id]
           view.emit('play', videoId, videoTitle)
       }
    })
    socket.on('setVolume', volume => {
        for(let id in views) {
            const view = views[id]
            view.emit('setVolume', volume)
        }
    })
    socket.on('pauseVideo', () => {
        for(let id in views) {
            const view = views[id]
            view.emit('pauseVideo')
        }
    })
    socket.on('nextVideo', () => {
        for(let id in views) {
            const view = views[id]
            view.emit('nextVideo')
        }
    })
    socket.on('videoInfo', (videoTitle, duration) => {
        for(let id in views) {
            const view = views[id]
            view.emit('videoInfo', videoTitle, duration)
        }
    })
    socket.on('videoProgress', progress => {
        for(let id in views) {
            const view = views[id]
            view.emit('videoProgress', progress)
        }
    })
    socket.on('requestInfo', () => {
        for(let id in views) {
            const view = views[id]
            view.emit('requestInfo')
        }
    })
    socket.on('disconnect', () => {
        if(views[socket.id]) {
            console.log("A view has left us")
            delete views[socket.id]
        }
    })
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, '../dist')))

app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname, '../dist/index.html'))
});

server.listen(3487, () => {
    console.log(`API running on localhost:3487`)
});